# Preprocess the data file
```python
ython preprocess.py --dataset <dataset_name>
```
# Traing the graph embedding
```python
python train_transe_mode.py --dataset <dataset_name>
```
# train RL agent 
```python
python train_agent.py --dataset <dataset_name>
```
# Evaluation
```python
python test_agent.py --dataset <dataset_name> --run_path Ture
```
## If "run_path" is True, the program will generate paths for recommendation according to the trained policy. If "run_eval" is True, the program will evaluate the recommendation performance based on the resulting paths.